@extends('roles.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    @hasPermission('role-create')
                    <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('roles.create') }}">Create New Role</a>
                </span>
                    @endhasPermission
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $roles)
                            <tr>
                                <td>{{ $roles->id }}</td>
                                <td>{{ $roles->name }}</td>
                                <td>
                                    @hasPermission('role-show')
                                    <a class="btn btn-info" href="{{ route('roles.show',$roles->id) }}">Show</a>
                                    @endhasPermission
                                    @hasPermission('role-update')
                                    <a class="btn btn-warning" href="{{ route('roles.edit',$roles->id) }}">Edit</a>
                                    @endhasPermission
                                    @hasPermission('role-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $roles->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
                                    @endhasPermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection
