@extends('roles.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                <div class="card-header">
                    <ul class="navbar-nav  justify-content-end">
                        <li>
                            <a class="btn btn-primary" href="{{ route('roles.index') }}">Back to Roles List</a>
                        <li>
                        <li>
                            <h3 class="text-center"> Role Information</h3>
                        <li>
                    </ul>
                </div>
                <div class="card-body d-flex flex-column align-items-center">
                    <div class="form-group">
                        <strong>Role Name:</strong>
                        {{ $roles->name }}
                    </div>

                    <div class="form-group">
                        <strong>Permissions</strong>
                        <br/>
                        @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $permission)
                                <span>{{ $permission->name }}</span>
                                <br/>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
