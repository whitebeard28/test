@extends('roles.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <ul class="navbar-nav  justify-content-end">
                        <li>
                            <a class="btn btn-primary" href="{{ route('roles.index') }}">Back to Posts List</a>
                        </li>
                        <li>
                            <h3 class="text-center">Edit Role</h3>
                        </li>
                    </ul>
                </div>
                <div class="card-body d-flex flex-column align-items-center">
                    {!! Form::model($roles, ['route' => ['roles.update', $roles->id],'method' => 'PUT']) !!}
                    <div class="form-group">
                        <div class="col-md-8">
                            <div class="input-group input-group-static mb-4">
                                <strong>Role Name</strong>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <strong>Permissions:</strong>
                        <br/>
                        @foreach($permissions as $value)
                            <label>{{ Form::checkbox('permissions[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ $value->name }}</label>
                            <br/>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
