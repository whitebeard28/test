@extends('users.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                <div class="card-header">
                    <ul class="navbar-nav  justify-content-end">
                        <li>
                            <a class="btn btn-primary" href="{{ route('users.index') }}">Back to Users List</a>
                        <li>
                        <li>
                            <h3> User Information</h3>
                        <li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Name:</strong>
                        {{ $users->name }}
                    </div>
                    <div class="lead">
                        <strong>Email:</strong>
                        {{ $users->email }}
                    </div>
                    <div class="lead">
                        <strong>Role:</strong>
                        @if(!empty($users->getRoleNames()))
                            @foreach($users->getRoleNames() as $val)
                                <span>{{ $val }}</span>
                            @endforeach @endif </div>
                    <div class="lead">
                        <strong>Password:</strong>
                        ********
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
