@extends('users.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('message'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('message') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    @hasPermission('user-create')
                    <span class="float-right">
                <a class="btn btn-primary" href="{{ route('users.create') }}">Create New User</a>
            </span>
                    @endhasPermission
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $val)
                                            <label>{{ $val }}</label>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @hasPermission('user-show')
                                    <a href="{{ route('users.show',$user->id) }}" class="btn btn-info">Show</a>
                                    @endhasPermission
                                    @hasPermission('user-update')
                                    <a href="{{ route('users.edit',$user->id) }}" class="btn btn-warning">Edit</a>
                                    @endhasPermission
                                    @hasPermission('user-delete')
                                    {!! Form::open(['method' => 'delete','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
                                    @endhasPermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection
