@extends('users.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <ul class="navbar-nav  justify-content-end">
                        <li>
                            <a class="btn btn-primary" href="{{ route('users.index') }}">Back to Users List</a>
                        <li>
                        <li>
                            <h3>Create New User</h3>
                        <li>
                    </ul>
                </div>

                <div class="card-body">

                    {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
                    <div class="form-group">
                        <div class="col-md-8">
                            <div class="input-group input-group-static mb-4">
                                <label>Name</label>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-static mb-4">
                            <div class="col-md-8">
                                <label>Email</label>
                                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-static mb-4">
                            <div class="col-md-8">
                                <label>Password</label>
                                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control', 'pattern'=>'.{8,}')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-static mb-4">
                            <div class="col-md-8">
                                <label>Confirm Password</label>
                                {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password','class' => 'form-control', 'pattern'=>'.{8,}')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <Label>Role</Label>
                            {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
