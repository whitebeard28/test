@extends('permissions.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <ul class="navbar-nav  justify-content-end">
                        <li>
                            <a class="btn btn-primary" href="{{ route('permissions.index') }}">Back to Permissions
                                List</a>
                        </li>
                        <li>
                            <h3 class="text-center">Edit Permission</h3>
                        </li>
                    </ul>
                </div>
                <div class="card-body d-flex flex-column align-items-center">
                    {!! Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method'=>'PUT']) !!}
                    <div class="form-group">
                        <div class="col-md-8">
                            <div class="input-group input-group-static mb-4">
                                <Label>Name</Label>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>

                    </
                    >
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
