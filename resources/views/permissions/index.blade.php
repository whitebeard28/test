@extends('permissions.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    <p>{{Session::get('message') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    @hasPermission('permission-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('permissions.create') }}">Create New Permission</a>
                    </span>
                    @endhasPermission
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $permission)
                            <tr>
                                <td>{{ $permission->id }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>
                                    @hasPermission('permission-show')
                                    <a class="btn btn-info" href="{{ route('permissions.show',$permission->id) }}">Show</a>
                                    @endhasPermission
                                    @hasPermission('permission-update')
                                    <a class="btn btn-warning" href="{{ route('permissions.edit',$permission->id) }}">Edit</a>
                                    @endhasPermission
                                    @hasPermission('permission-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $permission->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
                                    @endhasPermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
