@extends('permissions.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                <div class="card-header">
                    @can('role-create')
                        <ul class="navbar-nav  justify-content-end">
                            <li>
                                <a class="btn btn-primary" href="{{ route('permissions.index') }}">Back to Permissions
                                    List</a>
                            </li>
                            <li>
                                <h3 class="text-center">Permissions Information</h3>
                            </li>
                        </ul>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Name:</strong>
                        {{ $permission->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
