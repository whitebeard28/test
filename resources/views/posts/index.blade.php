@extends('posts.layouts.app')
@section('content')

    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('message'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('message') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    @hasPermission('post-create')
                    <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('posts.create') }}">Create New Post</a>
                </span>
                    @endhasPermission
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td>
                                    @hasPermission('post-show')
                                    <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Show</a>
                                    @endhasPermission
                                    @hasPermission('post-update')
                                    <a class="btn btn-warning" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                    @endhasPermission
                                    @hasPermission('post-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['posts.destroy', $post->id],'style'=>'display:inline']) !!}
                                    {!! Form::button('Delete', ['class' => 'btn btn-danger btn-delete']) !!}
                                    {!! Form::close() !!}
                                    @endhasPermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>



@endsection
