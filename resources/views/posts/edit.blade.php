@extends('posts.layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <ul class="navbar-nav  justify-content-end">
                    <li>
                        <a class="btn btn-primary" href="{{ route('posts.index') }}">Back to posts list</a>
                    </li>
                    <li>
                        <h3 class="text-center">Edit Post</h3>
                    </li>
                </ul>
            </div>
            <div class="card-body d-flex flex-column align-items-center">
                {!! Form::model($posts, ['route' => ['posts.update', $posts->id], 'method'=>'PUT']) !!}
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="input-group input-group-static mb-4">
                            <Label>Title</Label>
                            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="input-group input-group-static mb-4">
                            <Label>Body</Label>
                            {!! Form::textarea('body', null, array('placeholder' => 'Body','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
