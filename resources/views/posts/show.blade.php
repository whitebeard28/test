@extends('posts.layouts.app')
@section('content')

<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <ul class="navbar-nav  justify-content-end">
                    <li>
                        <a class="btn btn-primary" href="{{ route('posts.index') }}">Back to Posts List</a>
                    </li>
                    <li>
                        <h3>Post Information</h3>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="lead">
                    <strong>Title:</strong>
                    {{ $posts->title }}
                </div>
                <div class="lead">
                    <strong>Body:</strong>
                    {{ $posts->body }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
