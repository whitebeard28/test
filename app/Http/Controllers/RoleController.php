<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $data = Role::orderBy('id', 'ASC')->paginate(5);

        return view('roles.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $permissions = Permission::get();

        return view('roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoleRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRoleRequest $request): RedirectResponse
    {
        $dataCreate = $request->input();
        $roles = Role::create($dataCreate);
        $permission_id = $dataCreate['permissions'];

        $roles->permissions()->attach($permission_id);

        return redirect()->route('roles.index')
            ->with('message', 'Role created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $roles = Role::findOrFail($id);
        $rolePermissions = Permission::join('permission_role', 'permission_role.permission_id', 'permissions.id')
            ->where('permission_role.role_id', $id)
            ->get();

        return view('roles.show', compact('roles', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $roles = Role::findOrFail($id);
        $permissions = Permission::get();
        $rolePermissions = DB::table('permission_role')
            ->where('permission_role.role_id', $id)
            ->pluck('permission_role.permission_id', 'permission_role.permission_id')
            ->all();

        return view('roles.edit', compact('roles', 'permissions', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoleRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $dataUpdate = $request->all();
        $roles = Role::findOrFail($id);
        $roles->update($dataUpdate);
        $permission_id = $dataUpdate['permissions'];

        $roles->permissions()->detach();
        $roles->permissions()->attach($permission_id);

        return redirect()->route('roles.index')
            ->with('message', 'Role updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $roles = Role::findOrFail($id);
        $roles->delete();
        $roles->permissions()->detach();

        return redirect()->route('roles.index')
            ->with('message', 'Role deleted successfully');
    }
}
