<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $data = User::orderBy('id', 'desc')->paginate(5);

        return view('users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id')->all();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserQuest $request
     * @return RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {
        $dataCreate = $request->all();
        $users = User::create($dataCreate);
        $role_id = $dataCreate['roles'];

        $users->roles()->attach($role_id);

        return redirect()->route('users.index')
            ->with('message', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $users = User::findOrFail($id);

        return view('users.show', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $users = User::findOrFail($id);
        $roles = Role::pluck('name', 'id')->all();
        $userRoles = $users->roles->pluck('name', 'id')->all();

        return view('users.edit', compact('users', 'roles', 'userRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @param $role_id
     * @return RedirectResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $dataUpdate = $request->all();
        $user = User::findOrFail($id);
        $user->update($dataUpdate);
        $role_id = $dataUpdate['roles'];

        $user->roles()->detach();
        $user->roles()->attach($role_id);
        return redirect()->route('users.index')
            ->with('message', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        $users->roles()->detach();

        return redirect()->route('users.index')
            ->with('message', 'User deleted successfully.');
    }
}
