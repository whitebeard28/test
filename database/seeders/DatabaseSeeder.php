<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::updateOrCreate(['name' => 'admin'], ['display_name' => 'Người quản trị']);
        $roleUser = Role::updateOrCreate(['name' => 'user'], ['display_name' => 'Khách hàng']);
        $roleManagementUser = Role::updateOrCreate(['name' => 'management-user'], ['display_name' => 'Quản lí người dùng']);

        $permissionCreateUser = Permission:: updateOrCreate(['name' => 'user-create'], ['display_name' => 'Tạo người dùng']);
        $permissionUpdateUser = Permission:: updateOrCreate(['name' => 'user-update'], ['display_name' => 'Cập nhật người dùng']);
        $permissionShowUser = Permission:: updateOrCreate(['name' => 'user-show'], ['display_name' => 'Hiển thị người dùng']);
        $permissionDeleteUser = Permission:: updateOrCreate(['name' => 'user-delete'], ['display_name' => 'Xóa người dùng']);

        $permissionCreateRole = Permission:: updateOrCreate(['name' => 'role-create'], ['display_name' => 'Tạo role']);
        $permissionUpdateRole = Permission:: updateOrCreate(['name' => 'role-update'], ['display_name' => 'Cập nhật role']);
        $permissionShowRole = Permission:: updateOrCreate(['name' => 'role-show'], ['display_name' => 'Hiển thị role']);
        $permissionDeleteRole = Permission:: updateOrCreate(['name' => 'role-delete'], ['display_name' => 'Xóa role']);

        $permissionCreatePermission = Permission:: updateOrCreate(['name' => 'permission-create'], ['display_name' => 'Tạo quyền']);
        $permissionUpdatePermission = Permission:: updateOrCreate(['name' => 'permission-update'], ['display_name' => 'Cập nhật quyền']);
        $permissionShowPermission = Permission:: updateOrCreate(['name' => 'permission-show'], ['display_name' => 'Hiển thị quyền']);
        $permissionDeletePermission = Permission:: updateOrCreate(['name' => 'permission-delete'], ['display_name' => 'Xóa quyền']);

        $permissionCreatePost = Permission:: updateOrCreate(['name' => 'post-create'], ['display_name' => 'Tạo bài viết']);
        $permissionUpdatePost = Permission:: updateOrCreate(['name' => 'post-update'], ['display_name' => 'Cập nhật bài viết']);
        $permissionShowPost = Permission:: updateOrCreate(['name' => 'post-show'], ['display_name' => 'Hiển thị bài viết']);
        $permissionDeletePost = Permission:: updateOrCreate(['name' => 'post-delete'], ['display_name' => 'Xóa bài viết']);

        $roleManagementUser->permissions()->attach([$permissionCreateUser->id, $permissionUpdateUser->id, $permissionShowUser->id, $permissionDeleteUser->id]);
        $roleUser->permissions()->attach([$permissionCreatePost->id, $permissionUpdatePost->id, $permissionShowPost->id, $permissionDeletePost->id]);
        $roleAdmin->permissions()->attach([$permissionCreateUser->id, $permissionUpdateUser->id, $permissionShowUser->id, $permissionDeleteUser->id,
            $permissionCreateRole->id, $permissionUpdateRole->id, $permissionShowRole->id, $permissionDeleteRole->id, $permissionCreatePermission->id,
            $permissionUpdatePermission->id, $permissionShowPermission->id, $permissionDeletePermission->id,
            $permissionCreatePost->id, $permissionUpdatePost->id, $permissionShowPost->id, $permissionDeletePost->id]);

        $admin = User::whereEmail('whitebeard@gmail.com')->first();

        if (!$admin) {
            $admin = User::create([
                'name' => 'Hao Nguyen',
                'email' => 'whitebeard@gmail.com',
                'password' => '12345678'
            ]);
        }

        $admin->roles()->sync($roleAdmin->id);
    }
}

