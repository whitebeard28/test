<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome.welcome');
});


Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('users')->name('users.')->middleware('auth')->group(function () {
    Route::controller(UserController::class)->group(function () {
        Route::get('/', 'index')->name('index')->middleware(['check_permission: user-show']);
        Route::get('/create', 'create')->name('create')->middleware(['check_permission:user-create']);
        Route::post('/', 'store')->name('store')->middleware(['check_permission:user-create']);
        Route::get('/{id}', 'show')->name('show')->middleware(['check_permission:user-show']);
        Route::get('{id}/edit', 'edit')->name('edit')->middleware(['check_permission:user-update']);
        Route::put('/{id}/update', 'update')->name('update')->middleware(['check_permission:user-update']);
        Route::delete('/{id}', 'destroy')->name('destroy')->middleware(['check_permission:user-delete']);
    });
});
Route::prefix('roles')->name('roles.')->middleware('auth')->group(function () {
    Route::controller(RoleController::class)->group(function () {
        Route::get('/', 'index')->name('index')->middleware(['check_permission:role-show']);
        Route::get('/create', 'create')->name('create')->middleware(['check_permission:role-create']);
        Route::post('/', 'store')->name('store')->middleware(['check_permission:role-create']);
        Route::get('/{id}', 'show')->name('show')->middleware(['check_permission:role-show']);
        Route::get('/{id}/edit', 'edit')->name('edit')->middleware(['check_permission:role-update']);
        Route::put('/{id}/update', 'update')->name('update')->middleware(['check_permission:role-update']);
        Route::delete('/{id}/destroy', 'destroy')->name('destroy')->middleware(['check_permission:role-delete']);
    });
});
Route::prefix('permissions')->name('permissions.')->middleware('auth')->group(function () {
    Route::controller(PermissionController::class)->group(function () {
        Route::get('/', 'index')->name('index')->middleware(['check_permission:permission-show']);
        Route::get('/create', 'create')->name('create')->middleware(['check_permission: permission-create']);
        Route::post('/', 'store')->name('store')->middleware(['check_permission: permission-create']);
        Route::get('/{id}', 'show')->name('show')->middleware(['check_permission: permission-show']);
        Route::get('/{id}/edit', 'edit')->name('edit')->middleware(['check_permission: permission-update']);
        Route::put('/{id}/update', 'update')->name('update')->middleware(['check_permission: permission-update']);
        Route::delete('/{id}', 'destroy')->name('destroy')->middleware(['check_permission: permission-delete']);
    });
});
Route::prefix('posts')->name('posts.')->middleware('auth')->group(function () {
    Route::controller(PostController::class)->group(function () {
        Route::get('/', 'index')->name('index')->middleware(['check_permission: post-show']);
        Route::get('/create', 'create')->name('create')->middleware(['check_permission: post-create']);
        Route::post('/', 'store')->name('store')->middleware(['check_permission: post-create']);
        Route::get('/{id}', 'show')->name('show')->middleware(['check_permission: post-show']);
        Route::get('/{id}/edit', 'edit')->name('edit')->middleware(['check_permission: post-update']);
        Route::put('/{id}/update', 'update')->name('update')->middleware(['check_permission: post-update']);
        Route::delete('/{id}', 'destroy')->name('destroy')->middleware(['check_permission: post-delete']);
    });
});
